import Article from '../../../models/article.model.js'
import { baseAPI } from '../../routes.js'

/**
 * @param { import('express').Request } req
 * @param { import('express').Response } res
 */
export default async function getArticle (req, res) {
  try {
    const article = await Article.get(parseInt(req.params.id), req.query.truncate)
    res.status(200).json(article.asResource(baseAPI(req)))
  } catch (err) {
    res.status(404).json(err.message)
  }
}
