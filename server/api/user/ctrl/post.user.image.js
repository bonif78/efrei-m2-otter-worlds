import fs from 'fs'
import path from 'path'
import { promisify } from 'util'
import sharp from 'sharp'

const fsAccess = promisify(fs.access)
const fsMkdir = promisify(fs.mkdir)
const fsUnlink = promisify(fs.unlink)

/**
 * @param { import('express').Request } req
 * @param { import('express').Response } res
 */
export default async function postMapImage (req, res) {
  const image = req.file
  try {
    const pathURI = `users/${req.params.id}/`
    try {
      await fsAccess(path.join('static-back/back/', pathURI), fs.constants.W_OK)
    } catch {
      await fsMkdir(path.join('./static-back/back/', pathURI), { recursive: true })
    }

    await sharp(image.path)
      .resize(10000, 10000, { fit: 'inside', withoutEnlargement: true })
      .toFile(`./static-back/back/${pathURI}/user.jpg`)
    res.status(201).json(`/back/${pathURI}/user.jpg`)
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.code)
    const jsonErr = { code: err.code, message: 'Error while creating the new template.\n' }

    if (err.code === 'ER_NO_REFERENCED_ROW_2') {
      jsonErr.message += 'No existing foreigner for a given id.\n'
    } else if (err.code === 'ER_PARAMETER_UNDEFINED') {
      jsonErr.message += 'Missing a parameter.\n'
    } else if (err.code === 'ER_DUP_ENTRY') {
      jsonErr.message += 'Duplicate of a unique row.\n'
    }
    jsonErr.message += 'Please verify that your data is valid !'

    res.status(400).json(jsonErr)
  } finally {
    await fsUnlink(image.path)
  }
}
