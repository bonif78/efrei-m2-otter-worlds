import { Router } from 'express'
import multer from 'multer'

import isConnected from '../../middlewares/is-connected.js'
import passwordConfirmation from '../../middlewares/password-confirmation.js'
import { tryTo, emptyError } from '../../middlewares/errors.js'
import CharacterPolicy from '../../policies/character.policy.js'

import getCharacters from './ctrl/get.characters.js'
import getCharacter from './ctrl/get.character.js'
import getCharacterGroups from './ctrl/get.character.groups.js'
import getCharacterInventories from './ctrl/get.character.inventories.js'
import getCharacterStats from './ctrl/get.character.stats.js'
import postCharacter from './ctrl/post.character.js'
import postCharacterGroup from './ctrl/post.character.group.js'
import postCharacterImage from './ctrl/post.character.image.js'
import putCharacter from './ctrl/put.character.js'
import putCharacterStats from './ctrl/put.character.stats.js'
import putCharacterStatus from './ctrl/put.character.status.js'
import putCharacterStatusGM from './ctrl/put.character.status.gameMaster.js'
import deleteCharacter from './ctrl/delete.character.js'
import deleteCharacterGroup from './ctrl/delete.character.group.js'

function fileFilter (req, file, cb) {
  const allowedTypes = ['image/jpeg', 'image/png']

  if (!allowedTypes.includes(file.mimetype)) {
    const error = new Error('Wrong file type !')
    error.code = 'LIMIT_FILE_TYPES'
    return cb(error, false)
  }

  cb(null, true)
}

const upload = multer({
  dest: './temp',
  fileFilter,
  limits: {
    fileSize: 20000000 // 20Mo
  }
})

const {
  canGetUniverse,
  canGetUniverseIndirect,
  canEditUniverseIndirect,
  canEditUniverseIndirectSkip,
  isUser,
  isUserIndirect,
  verifyStat
} = require('../../middlewares/access-rights.js')

const canGet = canGetUniverseIndirect(CharacterPolicy.getUniverseId, 'id', 'params')
const canEditUniverse = canEditUniverseIndirect(CharacterPolicy.getUniverseId, 'id', 'params')
const canEditUniverseSkip = canEditUniverseIndirectSkip(CharacterPolicy.getUniverseId, 'id', 'params')
const canEditCharacter = isUserIndirect(CharacterPolicy.getUserId, 'id', 'params')

const router = Router()

// Get
router.get('/', tryTo(getCharacters, emptyError))
router.get('/:id', canGet, tryTo(getCharacter, emptyError))
router.get('/:id/groups', isConnected, canEditUniverse, tryTo(getCharacterGroups, emptyError))
router.get('/:id/inventories', canGet, tryTo(getCharacterInventories, emptyError))
router.get('/:id/stats', canGet, tryTo(getCharacterStats, emptyError))

// Post
router.post('/', isConnected, canGetUniverse('idUniverse', 'body'), isUser('idUser', 'body'), tryTo(postCharacter, emptyError))
router.post('/:id/groups', isConnected, canEditUniverse, tryTo(postCharacterGroup, emptyError))
router.post('/:id', upload.single('character-image'), tryTo(postCharacterImage, emptyError))

// Put
router.put('/:id', isConnected, canEditCharacter, tryTo(putCharacter, emptyError))
router.put('/:id/status', isConnected, canEditUniverseSkip, tryTo(putCharacterStatusGM, emptyError))
router.put('/:id/status', isConnected, canEditCharacter, tryTo(putCharacterStatus, emptyError))
router.put('/:id/stats', isConnected, canEditCharacter, verifyStat('id', 'params'), tryTo(putCharacterStats, emptyError))

// Delete
router.delete('/:id', isConnected, canEditCharacter, passwordConfirmation, tryTo(deleteCharacter, emptyError))
router.delete('/:id/groups', isConnected, canEditUniverse, tryTo(deleteCharacterGroup, emptyError))

export default router
