import Character from '../../../models/character.model.js'

/**
 * @param { import('express').Request } req
 * @param { import('express').Response } res
 */
export default async function putCharacterStatus (req, res) {
  const sheetStatus = parseInt(req.body.sheetStatus)
  if (sheetStatus !== 0 && sheetStatus !== 1) { res.sendStatus(401); return }
  try {
    await Character.updateSheetStatus(parseInt(req.params.id), sheetStatus)
    res.status(200).json(true)
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.code)
    const jsonErr = { code: err.code, message: 'Error while updating the new character.\n' }

    if (err.code === 'ER_PARAMETER_UNDEFINED') {
      jsonErr.message += 'Missing a parameter.\n'
    } else if (err.code === 'ER_DUP_ENTRY') {
      jsonErr.message += 'Duplicate of a unique row.\n'
    }
    jsonErr.message += 'Please verify that your data is valid !'

    res.status(400).json(jsonErr)
  }
}
